# -*- coding:utf-8 -*-
from __future__ import print_function
import os
import stat

path = os.getcwd()

for (path, dirs, files) in os.walk(path):
    for File in files:
        if File.endswith('.pyc'):
            try:
                os.remove(os.path.join(path, File))

            except IOError:
                fileAtt = os.stat(os.path.join(path, File))[0]
                if (not fileAtt & stat.S_IWRITE):
                    # File is read-only, so make it writeable,
                    # this gets us around Synergy:)
                    os.chmod(os.path.join(path, File), stat.S_IWRITE)
                os.remove(os.path.join(path, File))
print("Complete")
