"""
mds.messages.py
collection of messages defined in 
SPN041-0002 MDS Message Specification v2.7
Created on 22 Jan 2016

@author: JMoore
Copyright 2016 Raymarine UK Limited
Commercial in confidence
"""


class MdsBase(object):
    '''
    Defines the various variants of MDS message
    and the minimum content of those messages
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        pass


class MdsRole(MdsBase):
    '''
    Defines the mds roles that devices that transmit 
    this message can partake in:
        Source
        Consumer
        Orchestrator
    A device can have one or more roles in the MDS scheme
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        pass
