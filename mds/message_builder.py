
def build_roles(**kwargs):
    message = CSTIIMdsRoles()
    numServices = config.get('number_of_roles', 0)
    message.SetValue('roles to follow', numServices)
    for n in range(numServices):
        message.SetValue(
            "{} Role".format(dElementOrder[n]),
            dRoleEncode[config.get('role_{}'.format(n + 1), 'dna')])
    return message


def build_sources(**kwargs):
    message = CSTIIMdsServiceSource()
    numServices = config.get('number_of_sources', 0)
    message.SetValue('Sources To Follow', numServices)
    for n in range(numServices):
        message.SetValue(
            "{} Source".format(dElementOrder[n]),
            dServices[config.get('source_{0:d}'.format(n + 1),
                                      'dna')])
    return message


def build_consumers(**kwargs):
    message = CSTIIMdsServiceConsumer()
    numServices = config.get('number_of_consumers', 0)
    message.SetValue('Consumers To Follow', numServices)
    for n in range(numServices):
        message.SetValue(dElementOrder[n] + " Consumer", dServices[
            config.get('consumer_{0:d}'.format(n + 1), 'dna')])
    return message


def build_service_instance_config(**kwargs):
    message = CSTIIMdsServiceInstanceConfiguration()
    message.SetValue(
        'Orchestration Event Unique Number',
        config.get('unique_number', 0))
    message.SetValue(
        'Service ID', dServices[config.get(
            'service_id', 'dna')])
    message.SetValue(
        'Selection Rule', dSelectionRule[config.get(
            'selection_rule', 'dna')])
    message.SetValue('Number Of Sources', 1)
    numServices = config.get('transport_layers', 0)
    message.SetValue(
        'Number Of Transport Layers To Follow',
        numServices)
    for n in range(numServices):
        message.SetValue(
            '{0} Length Of Transport Data'.format(
                dElementOrder[n]),
            config.get(
                'transport_length_{0:d}'.format(n + 1), 0))
        message.SetValue(
            '{0} Transport Data Type'.format(dElementOrder[n]),
            dTransportLayer[config.get(
                'transport_data_type_{0:d}'.format(n + 1),
                'dna')])
        message.SetValue(
            '{0} Transport Data'.format(dElementOrder[n]),
            config.get(
                'transport_data_{0:d}'.format(n + 1), 0))
    return message


def build_service_instance_ack(**kwargs):
    message = CSTIIMdsServiceInstanceConfigurationAck()
    message.SetValue(
        'Orchestration Event Unique Number',
        config.get('unique_number', 0))
    message.SetValue(
        'Service ID', dServices[config.get(
            'service_id', 'dna')])
    message.SetValue(
        'Acceptance', dAcceptance[config.get(
            'acceptance', 'dna')])
    numServices = config.get('failure_reasons', 0)
    message.SetValue(
        'Failures Reasons', numServices)
    for n in range(numServices):
        message.SetValue(
            "{0} Failure Reason".format(dElementOrder[n]),
            dFailureReasons[
                config.get('failure_reason_{0:d}'.format(n + 1),
                                'dna')])
    return message


def build_commit_changes(**kwargs):
    message = CSTIIMdsCommitChanges()
    message.SetValue('Commit Type', config.get(
        'commit_type', 1))
    message.SetValue(
        'Commit Action', dCommit[config.get(
            'commit_action', 'dna')])
    numServices = config.get('number_of_commits', 0)
    message.SetValue(
        'Number Of Orchestration Events To Commit', numServices)
    for n in range(numServices):
        message.SetValue(
            "{0} Orchestration Event Unique Number".format(
                dElementOrder[n]),
            config.get('unique_number_{0:d}'.format(n + 1), 0))
        message.SetValue(
            "{0} Service Id To Be Defined".format(
                dElementOrder[n]),
            dServices[config.get(
                'service_id_{0:d}'.format(n + 1), 'dna')])
    return message


def build_bridged_service_sample_data(**kwargs):
    message = CSTIIMdsBridgedServiceSampleData()
    message.SetValue(
        'Service ID', dServices[config.get(
            'service_id', 'dna')])
    numServices = config.get('sources_remaining', 255)
    message.SetValue('Sources Remaining', numServices)
    message.SetValue(
        'Sample Data Type', dSampleDataType[config.get(
            'sample_data_type', 'dna')])
    message.SetValue(
        'Sample Data Length', config.get(
            'sample_data_length', 0))
    message.SetValue('Sample Data', config.get(
        'sample_data', 0))
    message.SetValue(
        'Length Of Transport Data', config.get(
            'transport_length', 0))
    message.SetValue('Transport Data Type', dTransportLayer[
                     config.get(
                         'transport_data_type', 'dna')])
    message.SetValue('Transport Data', config.get(
        'transport_data', 0))
    message.SetValue('In Use', config.get(
        'in_use', 0))
    return message
