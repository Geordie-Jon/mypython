from collections import defaultdict
from nmea import ISO
import logging

logging.DEV = 5
logging.addLevelName(logging.DEV, 'DEV')

def dev(self, message, *args, **kwargs):
    self.log(logging.DEV, message, *args, **kwargs) 
logging.Logger.dev = dev

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)


class SimulatedDevice(object):
    def __init__(self, a_comm_object=None, device_info=None):
        super(SimulatedDevice,self).__init__()
        self._setup_nmea(device_info)
        self._setup_comms(a_comm_object)

    def _setup_comms(self, a_comm_object):
        self.comms=a_comm_object
        self.tx_message = self.comms.TxMessage
        self._start_bus_listener()
        self.comms.RegisterMessageProcessor(
            59904, self._iso_request_handler)
        self.comms.RegisterMessageProcessor(
            60928, self._address_claim_handler)
        self.send_address_claim()

    def _setup_nmea(self, device_info):        
        self.device_info = device_info
        self.iso = ISO(self.device_info)
        
        
    def _start_bus_listener(self):
        '''Creates the device's view of the world
        This mapping will be used to track and manage
        communications by registering can addresses
        Specific callbacks must also check the mapping
        '''
        self.devices_on_network = defaultdict(dict)
        try:
            self.comms.RegisterDefaultMessageProcessor(
                self._add_network_device)
        except AttributeError as e:
            if self.comms is not None:
                raise
            else:
                logging.debug('Not registered as comms is None')
        
    def _add_network_device(self, message):
        '''Monitors all transmissions on network and
        creates a mapping of source addresses to network information
        '''
        can_address = message.GetSourceAddress()
        # log.dev('Received SA={0:X}'.format(can_address))
        self.devices_on_network[can_address]
        # if empty then request product info
        if not self.devices_on_network[can_address]:
            request_for_product_info = self.iso.request(126996)
            self.tx_message(request_for_product_info,
                                 destination=can_address)
            self.devices_on_network[
                can_address]['product information'] = 'Not set'

    def _iso_request_handler(self, request):
        pgn = request.GetValue('PGN')

    def _get_address(self):
        return self.comms.get_address()
    
    def _set_address(self, new_address):
        self.comms.set_address(new_address)
        
    def _address_claim_handler(self, address_claim):
        if not self.iso.address_claim_success(address_claim):
            self._set_address(self._get_address()+1)


## Public methods

   
    def send_address_claim(self, target=0xFF):
        self.tx_message(
            self.iso.address_claim(), destination=target)
           
    def send_iso_request(self, pgn, target):
        self.tx_message(
            self.iso.request(pgn), destination=target)
        
