import os
import unittest as rayunit
from test import test_transmitter
os.system(('clear','cls')[os.name=='nt'])
test_transmitter.log.info('Starting Tests\n')
suite = rayunit.TestLoader().discover(start_dir='test')
rayunit.TextTestRunner(verbosity=2).run(suite)
