'''
Module to implement nmea2000 ISO functions
 Address Claim
 Request
 Command Group Function
 Request Group Function
 Acknowledge Group Function
 ...
 
 '''
import logging
import message_builder


class ISO(object):
    def __init__(self, device_info):
        self.device_info = device_info # mutable reference to owner
        
    def address_claim(self):
        '''place a claim on the address'''
        return message_builder.build_address_claim(**self.device_info)

    def address_claim_success(self, other):
        '''negotiate for the can address'''
        return self < other 

    def request(self, pgn):
        '''want a pgn? this is how'''
        return message_builder.build_iso_request(pgn)
