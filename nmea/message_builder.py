from Library.comms.CAN.NMEA2000_messages.nmea2000ISO import *
def build_ProprietaryAck():
    message = CSTII_ProprietaryAcknowledgement()
    message.SetValue('Proprietary ID Acknowledged', 0xffff)
    message.SetValue('Protocol Acknowledged', "Single Frame")
    message.SetValue('Ack Status', "Data Not Available")
    message.SetValue('Acknowledgment Parameter', 0xfffff)
    return message

def build_pgn_list(direction, pgn_list):
    message = CNMEA2000PGNList()
    message.SetValue('PGN Group Function Code', direction)
    message.SetList(pgn_list)
    return message

def build_rx_list(**kwargs):
    # (59904, 60928, 61184, 126208, 126996,)))
    return build_pgn_list(1,pgn_list)


def build_tx_list(pgn_list):
    # (59904, 60928, 61184, 126464, 126996, 126720)))
    return build_pgn_list(0,pgn_list)


def build_iso_request(pgn):
    message = CNMEA2000ISORequestMessage()
    message.SetValue('PGN', pgn)
    return message


def build_address_claim(**kwargs):
    message = CNMEA2000ISOAddressClaimMessage()
    message.SetValue('Unique Number',
                     int(kwargs.get('serial_code',
                                         1111111)))
    message.SetValue('Manufacturer Code',
                     kwargs.get('manufacturer_code',
                                     1851))
    message.SetValue('Device Instance Lower',
                     kwargs.get('dev_instance_lo',
                                     0))
    message.SetValue('Device Instance Upper',
                     kwargs.get('dev_instance_hi',
                                     0))
    message.SetValue('Device Function',
                     kwargs.get('dev_function',
                                     1))
    message.SetValue('Dominant Bit',
                     kwargs.get('dominant_bit',
                                     0))
    message.SetValue('Device Class',
                     kwargs.get('dev_class',
                                     1))
    message.SetValue('System Instance',
                     kwargs.get('system_instance',
                                     252))
    message.SetValue('Industry Group',
                     kwargs.get('industry_group',
                                     4))
    message.SetValue('NMEA2000 Reserved (ISO Self Configurable)',
                     kwargs.get('iso_self_configurable',
                                     1))
    return message


def build_product_info(**kwargs):
    message = CNMEA2000ProductInformationMessage()
    message.SetValue(
        'Database Version', kwargs.get(
            'database_version', 0x2100))
    message.SetValue(
        'Manufacturers Product Code', kwargs.get(
            'manufacturer_code', 1851))
    message.SetValue(
        'Manufacturers Model ID', kwargs.get(
            'model_id', '0'))
    message.SetValue(
        'Manufacturers Software Version Code', kwargs.get(
            'software_version', '0'))
    message.SetValue(
        'Manufacturers Model Version', kwargs.get(
            'model_version', '0'))
    message.SetValue(
        'Manufacturers Model Serial Code', str(kwargs.get(
            'serial_code', '1111111')))
    message.SetValue(
        'Certification Level', kwargs.get(
            'certification_level', 0xff))
    message.SetValue(
        'Load Equivalency Number', kwargs.get(
            'load_equivalence', 0xff))
    return message

