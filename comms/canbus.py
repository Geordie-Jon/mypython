"""
Created on 22 Jan 2016

@author: JMoore
Copyright 2016 Raymarine UK Limited
Commercial in confidence
"""


class CanBus(object):
    '''
    classdocs
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        pass

    def send(self, can2bExtended):
        '''desegments and transmits a can2bExtended message'''
        for frame in can2bExtended:
            self._send_frame(frame)

    def recv(self):
        '''returns a constructed can2bExtended message'''
        pass
