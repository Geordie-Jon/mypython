import unittest as rayunit
import time
import struct
from transmitters import SimulatedDevice
from Library.comms.CAN.NMEA2000_messages.nmea2000steering\
     import CNMEA2000VesselHeadingMessage as vhm
from Library import CNMEA2000CanCommunication
from utilities.transmitter_project.canport_object import PortObject
import logging
log = logging.getLogger(__name__)



# A transmitter may (not #TODO: ?) be instantiated directly

class TransmitterTestBase(rayunit.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.comms = PortObject(channel=0)

    @classmethod
    def tearDownClass(cls):
        cls.comms.StopRx()

class TransmitterTests(TransmitterTestBase):
        
    def test_creation(self):
        test_device = SimulatedDevice(self.comms,{})
        self.assertIsNotNone(test_device)

    def test_has_a_comm_port(self):
        test_device = SimulatedDevice(self.comms,{})
        self.assertTrue(
            hasattr(test_device,'comms'), 'No comm object')

    def test_has_an_actual_comm_port(self):
        self.test_device = SimulatedDevice(self.comms,{})
        self.assertIsNotNone(self.test_device.comms)


class TransmitterPropertyTests(TransmitterTestBase):
        
    def setUp(self):
        self.test_device = SimulatedDevice(self.comms,{})

    def test_has_network_mapping(self):
        self.assertTrue(
            hasattr(self.test_device,'devices_on_network'),
                'No devices mapping')

    def test_sources_are_added_to_mapping(self):
        '''Emits a vessel heading message and verifies the
        source address is added to the mapping
        '''
        test_can_address = 0xBA
        second_port = CNMEA2000CanCommunication(
            channel=0) # because we do not get callbacks on what we transmit
        heading = vhm()
        heading.SetSourceAddress(test_can_address)
        heading.TxMessage(second_port)
        time.sleep(1)
        self.assertIn(test_can_address,
                      self.test_device.devices_on_network,
                      self.test_device.devices_on_network
                      )
        second_port.StopRx()
        log.dev(self.test_device.devices_on_network)
        
    def tearDown(self):
        pass
        #log.info(self.test_device.devices_on_network)
