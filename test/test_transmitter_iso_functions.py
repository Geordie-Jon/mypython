'''
Module to verify and maintain tests for nmea2000 ISO functions
 Address Claim
 Request
 Command Group Function
 Request Group Function
 Acknowledge Group Function
 ...
 
 '''
import traceback as tb
from nmea.message_builder import CNMEA2000ISOAddressClaimMessage
from transmitters import SimulatedDevice
from test_transmitter import (TransmitterTestBase,
                              CNMEA2000CanCommunication
                              )
import logging
log = logging.getLogger(__name__)


class TransmitterRegistrationTests(TransmitterTestBase):
        
    def setUp(self):
        self.test_device = SimulatedDevice(self.comms,
                                           {'source_address':0xBA})
        self.observer_port = CNMEA2000CanCommunication(channel=0)
        self.observer_port.RegisterMessageProcessor(60928,
                                                    self.received)

    def tearDown(self):
        self.observer_port.StopRx()

    def received(self, message):
        log.debug(message)

    def test_registering_callbacks(self):
        pass

    def test_building_an_address_claim(self):
##        self.assertIsNotNone(self.test_device.iso.address_claim())
        self.assertIsInstance(
            self.test_device.iso.address_claim(),
            CNMEA2000ISOAddressClaimMessage
            )

        
    
    def test_emitting_an_address_claim(self):
        self.assertIsNone(self.test_device.send_address_claim())

    def test_building_an_iso_request(self):
        self.assertIsNotNone(self.test_device.iso.request(60928))

    def test_emitting_an_iso_request(self):
        self.assertIsNone(self.test_device.send_iso_request(60928,204))
